import Config

# Configure your database
config :rit_server, RitServer.Repo,
  username: "postgres",
  password: "postgres",
  database: "rit_server_dev",
  hostname: "postgres",
  show_sensitive_data_on_connection_error: true,
  pool_size: 10

# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

import Config

wildcard_import = fn wildcard ->
  for config <- wildcard |> Path.expand(__DIR__) |> Path.wildcard() do
    import_config config
  end
end

# General application configuration
wildcard_import.("general/*.exs")

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
wildcard_import.("#{Mix.env()}/*.exs")

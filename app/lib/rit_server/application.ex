defmodule RitServer.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # Perform bootstrap before starting application
    RitServer.Tasks.bootstrap()

    # List all child processes to be supervised
    children = [
      # Start the Ecto repository
      RitServer.Repo,
      # Start the endpoint when the application starts
      RitServerWeb.Endpoint
      # Starts a worker by calling: RitServer.Worker.start_link(arg)
      # {RitServer.Worker, arg},
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: RitServer.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    RitServerWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end

import Config

# Configures the endpoint
config :rit_server, RitServerWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "w7qcgVBLSl18yYwFAaBYsOA1i5TFfqGyL5CMhAD7s18i5f6RQsrcMWMu5+yRTclz",
  render_errors: [view: RitServerWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: RitServer.PubSub, adapter: Phoenix.PubSub.PG2]

defmodule RitServer.Tasks.Database do
  @moduledoc """
  Database tasks are functions that integrate RitServer with PostgreSQL and
  execute changes on it.
  """

  @app :rit_server

  alias Ecto.Migrator

  require Logger

  def create(repeat_options \\ []) do
    Enum.each(repos(), fn repo ->
      try do
        case repo.__adapter__.storage_up(repo.config) do
          :ok ->
            Logger.info("The database for #{inspect(repo)} has been created")
            {:ok, :created}

          {:error, :already_up} ->
            Logger.info("The database for #{inspect(repo)} has already been created")

          {:error, term} ->
            raise "The database for #{inspect(repo)} couldn't be created: #{inspect(term)}"

          error ->
            raise "The database for #{inspect(repo)} couldn't be created: #{inspect(error)}"
        end
      rescue
        _error ->
          Logger.error("The database for #{inspect(repo)} couldn't be created.")
          handle_create_error(repeat_options)
      end
    end)
  end

  def migrate do
    Enum.each(repos(), fn repo ->
      {:ok, _, _} = Migrator.with_repo(repo, &Migrator.run(&1, :up, all: true))
    end)
  end

  def rollback(repo, version) do
    {:ok, _, _} = Migrator.with_repo(repo, &Migrator.run(&1, :down, to: version))
  end

  defp repos do
    Application.load(@app)
    Application.fetch_env!(@app, :ecto_repos)
  end

  defp handle_create_error(options) do
    if Keyword.get(options, :repeat?, false) == true do
      attempts = Keyword.get(options, :attempts, 0) + 1
      options = Keyword.put(options, :attempts, attempts)

      case Keyword.get(options, :until, :infinity) do
        :infinity -> inform_wait_and_attempt_to_create(options)
        until when is_number(until) -> inform_wait_and_attempt_to_create(options)
      end
    end
  end

  defp inform_wait_and_attempt_to_create(options) do
    attempts = Keyword.fetch!(options, :attempts)
    until = Keyword.get(options, :until, :infinity)

    if until == :infinity do
      Logger.warn("Attempt #{attempts} failed.")
    else
      if attempts >= until do
        raise "Attempt #{until} of #{until} failed."
      else
        Logger.warn("Attempt #{attempts} of #{until} failed.")
      end
    end

    waiting_milliseconds = Keyword.get(options, :wait_milliseconds, 5_000)

    Logger.info("Retrying in #{waiting_milliseconds} milliseconds.")

    :timer.sleep(waiting_milliseconds)

    create(options)
  end
end

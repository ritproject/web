import Config

# Configure your database
config :rit_server, RitServer.Repo,
  username: "postgres",
  password: "postgres",
  database: "rit_server_test",
  hostname: "postgres",
  pool: Ecto.Adapters.SQL.Sandbox

defmodule RitServer.Tasks do
  @moduledoc """
  Tasks are functions intended to be used to integrate external services or
  bootstrap components. It is used by RitServer.Application to bootstrap the
  database before the start of the application and can be executed using the
  eval command on release CLI:

  ```bash
  rit_server eval "RitServer.Tasks.function(params)"
  ```
  """

  alias RitServer.Tasks.Database

  @options Application.get_env(:rit_server, :rit_bootstrap, [])

  def bootstrap(options \\ []) do
    if Enum.empty?(options) do
      bootstrap_database(@options)
    else
      bootstrap_database(Keyword.merge(@options, options, &(&2 || &1)))
    end
  end

  def bootstrap_database(options) do
    create_result =
      case Keyword.get(options, :create_database) do
        nil -> :ok
        create_database_options -> Database.create(create_database_options)
      end

    if create_result == {:ok, :created} do
      Database.migrate()
    end
  end
end

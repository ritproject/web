defmodule RitServer.Repo do
  use Ecto.Repo,
    otp_app: :rit_server,
    adapter: Ecto.Adapters.Postgres
end
